// Config Files
var currentjob;

// Files
var fs = require('fs');
var path = require('path');
var clean = require('gulp-clean');

// Notifications
var notify = require('gulp-notify');

// Prompts
var prompts = require('gulp-prompt');

// GULP essentials
var gulp = require('gulp');
var less = require('gulp-less');
var smoosher = require('gulp-smoosher');
var inlineCss = require('gulp-inline-css');
var browsersync = require('browser-sync').create();
var sftp = require('gulp-sftp');
var runSequence = require('run-sequence');
var validate = require("validate-npm-package-name");
var jeditor = require("gulp-json-editor");
var uncss = require("gulp-uncss");
var nunjucksRender = require('gulp-nunjucks-render');
var litmus = require('gulp-litmus');
var concat = require('gulp-concat');
var replace = require('gulp-replace');
var insertLines = require('gulp-insert-lines');

var postcss = require('gulp-postcss');
var mqpacker = require("css-mqpacker");
var plumber = require("gulp-plumber");

// Global Variables
var arr = [];
var paths = [];

// Paths
var path = {
  base:       					'./',
	master: 							'./.master-email-framework',
	master_templates: 		'./.master-email-framework/templates',
	master_parts: 				'./.master-email-framework/parts',
	master_less: 					'./.master-email-framework/less',
	builds: 							'./builds'
}

//-------- Dev steps --------//
// + slice up ART
// + choose email sections
// + flow in content
// + style
// + upload images
// + absolutely link images

//-------- Test steps --------//
// + send to Email on Acid
// + repeat till email looks great in all clients!!
// + grab final html
// + upload that final html to the cdn

// Creates current.json file if one does not exist
function current() {
	if(!fs.existsSync('.current.json')) {
		var job = '{ "vertical": "", "email_type": "", "campaign_year": "", "client": "", "campaign_month": "", "campaign_name": "" }';
		fs.writeFileSync('.current.json', job);
		currentjob = require('./.current.json');
		return currentjob;
	}

	currentjob = require('./.current.json');
	return currentjob;
}

// The Eye in the Sky
gulp.task('watch', ['google-fonts'], function() {
	var job_path = path.builds + '/' + currentjob.vertical + '/' + currentjob.campaign_year + '/' + currentjob.client + '/' + currentjob.campaign_month + "/" + currentjob.campaign_name;
	var currentpath = currentjob.vertical + '/' + currentjob.campaign_year + '/' + currentjob.client + '/' + currentjob.campaign_month + "/" + currentjob.campaign_name;

  browsersync.init({
  	server: {
  		baseDir: [job_path + '/final']
  	}
  });
  gulp.watch(job_path + '/less/**/*.less', ['google-fonts', browsersync.reload]);
  gulp.watch(job_path + '/index.html', ['inline', 'google-fonts', 'html-update', browsersync.reload]);
  gulp.watch('builds/' + currentpath + '/images/**', ['img-update', 'google-fonts', browsersync.reload]);
});

// Compile LESS files
gulp.task('less', function(){
	var job_path = path.builds + '/' + currentjob.vertical + '/' + currentjob.campaign_year + '/' + currentjob.client + '/' + currentjob.campaign_month + "/" + currentjob.campaign_name;

	return gulp.src([path.master_less + '/emailclients.less', job_path + '/less/inline.less'])
		.pipe(plumber({errorHandler: lessError}))
		.pipe(less({
			paths: path.master_less
		}))
		.pipe(uncss({
			html: [job_path + '/index.html'],
			ignore: ['.ReadMsgBody', '.ExternalClass', '.yshortcuts']
		}))
		.pipe(concat('email.css'))
		.pipe(postcss([mqpacker]))
		.pipe(gulp.dest(job_path + '/css'));
});

// Place CSS in HTML file
gulp.task('inline', ['less'], function(){
	var job_path = path.builds + '/' + currentjob.vertical + '/' + currentjob.campaign_year + '/' + currentjob.client + '/' + currentjob.campaign_month + '/' + currentjob.campaign_name;

  if ( currentjob.client == "atl" ) {
  	return gulp.src(job_path + '/index.html')
		.pipe(smoosher())
		.pipe(inlineCss({
			removeLinkTags: true,
			applyWidthAttributes: true,
      applyStyleTags: true,
			removeStyleTags: true,
      preserveMediaQueries: true
		}))
    .pipe(insertLines({
      'after': /<style>$/,
      'lineAfter': '/* uncss:ignore */\n.unsubscribe a { color: #4e4e4e !important; }\n/* uncss:ignore */\n.preferences a { color: #4e4e4e !important; }\n/* uncss:ignore */\n.no-blue a { color: #4e4e4e !important; }\n/* uncss:ignore */\nbody{margin: 0 auto;padding: 0;}\n/* uncss:ignore */\n.outlook-array .four { width: 175px !important; }\n@viewport {width: device-width !important;}\n/* uncss:ignore */\na[x-apple-data-detectors] {color: inherit !important;text-decoration: none !important;font-size: inherit !important;font-family: inherit !important;font-weight: inherit !important;line-height: inherit !important;}\n/* uncss:ignore */\n.ReadMsgBody { width: 100%; }\n/* uncss:ignore */\n.ExternalClass { width: 100%; }\n/* uncss:ignore */\n.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:100%;}\n/* uncss:ignore */\n.yshortcuts a { border-bottom: none !important; }\n/* uncss:ignore */\n.body-copy .copy img {display: inline-block;}\n/* uncss:ignore */\ndiv.outlook-hide table {width: 0 !important; height: 0 !important; overflow: hidden !important; display: none !important; visibility: hidden !important; mso-hide:all !important; max-height: 0 !important;font-size: 0 !important;line-height:0 !important;}\n/* uncss:ignore */\na {text-decoration: none;}\n'
    }))
		.pipe(gulp.dest(job_path + '/final'))
		.pipe(browsersync.stream());
  }
  else {
    return gulp.src(job_path + '/index.html')
		.pipe(smoosher())
		.pipe(inlineCss({
			removeLinkTags: true,
			applyWidthAttributes: true,
      applyStyleTags: true,
			removeStyleTags: true,
      preserveMediaQueries: true
		}))
    .pipe(insertLines({
      'after': /<style>$/,
      'lineAfter': '/* uncss:ignore */\nbody{margin: 0 auto;padding: 0;}\n/* uncss:ignore */\n.outlook-array .four { width: 175px !important; }\n@viewport {width: device-width !important;}\n/* uncss:ignore */\na[x-apple-data-detectors] {color: inherit !important;text-decoration: none !important;font-size: inherit !important;font-family: inherit !important;font-weight: inherit !important;line-height: inherit !important;}\n/* uncss:ignore */\n.ReadMsgBody { width: 100%; }\n/* uncss:ignore */\n.ExternalClass { width: 100%; }\n/* uncss:ignore */\n.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:100%;}\n/* uncss:ignore */\n.yshortcuts a { border-bottom: none !important; }\n/* uncss:ignore */\n.body-copy .copy img {display: inline-block;}\n/* uncss:ignore */\ndiv.outlook-hide table {width: 0 !important; height: 0 !important; overflow: hidden !important; display: none !important; visibility: hidden !important; mso-hide:all !important; max-height: 0 !important;font-size: 0 !important;line-height:0 !important;}\n/* uncss:ignore */\na {text-decoration: none;}\n'
    }))
		.pipe(gulp.dest(job_path + '/final'))
		.pipe(browsersync.stream());

  }
});

// Placing Google Fonts
gulp.task('google-fonts', ['inline'], function(){
	var job_path = path.builds + '/' + currentjob.vertical + '/' + currentjob.campaign_year + '/' + currentjob.client + '/' + currentjob.campaign_month + '/' + currentjob.campaign_name + '/final' ;
  var fonts = '';
  var overrides = '';
  var count = 0;
  var google = 0;

  var obj = JSON.parse(fs.readFileSync("./googlefonts.json"));

  fs.readFile(job_path + '/index.html', function (err, data) {
    if (err) throw err;

    // Creating the font call
    for( var i in obj ) {
      if(data.indexOf(i) >= 0){
        google++;
        if (!count) { fonts = 'https://fonts.googleapis.com/css?family='; count++; }
        else { fonts = fonts + '|'; }

        // font name (encoded)
        for( var j in obj[i][0] ) { fonts = fonts + obj[i][0][j]; }

        // font weights
        for( var j in obj[i][1] ) { fonts = fonts + ':' + obj[i][1][j]; }

        // font name (encoded)
        for( var j in obj[i][2] ) { overrides = overrides + "\n/* uncss:ignore */\n" + obj[i][2][j]; }
      }
  	}

    if(google) {
      return gulp.src(job_path + '/index.html')
      .pipe(insertLines({
        'after': /<style>$/,
        'lineAfter': overrides
      }))
      .pipe(insertLines({
        'after': /<head>$/,
        'lineAfter': '<style>\n@import url(\''+ fonts +'\');\n</style>'
      }))
  		.pipe(gulp.dest(job_path))
  		.pipe(browsersync.stream());
    }
  });
});

// Outlook rounded buttons
gulp.task('outlook-buttons', function(){
  current();

	var job_path = path.builds + '/' + currentjob.vertical + '/' + currentjob.campaign_year + '/' + currentjob.client + '/' + currentjob.campaign_month + '/' + currentjob.campaign_name + '/final' ;

  var obj = JSON.parse(fs.readFileSync("./googlefonts.json"));

  fs.readFile(job_path + '/index.html', function (err, data) {
    if (err) throw err;
    console.log(data);

    // Creating the font call
    for( var i in obj ) {
    //   if(data.indexOf(i) >= 0){
    //     google++;
    //     if (!count) { fonts = 'https://fonts.googleapis.com/css?family='; count++; }
    //     else { fonts = fonts + '|'; }
    //
    //     // font name (encoded)
    //     for( var j in obj[i][0] ) { fonts = fonts + obj[i][0][j]; }
    //
    //     // font weights
    //     for( var j in obj[i][1] ) { fonts = fonts + ':' + obj[i][1][j]; }
    //
    //     // font name (encoded)
    //     for( var j in obj[i][2] ) { overrides = overrides + "\n/* uncss:ignore */\n" + obj[i][2][j]; }
    //   }
  	}
    //
    // if(google) {
    //   return gulp.src(job_path + '/index.html')
    //   .pipe(insertLines({
    //     'after': /<style>$/,
    //     'lineAfter': overrides
    //   }))
    //   .pipe(insertLines({
    //     'after': /<head>$/,
    //     'lineAfter': '<style>\n@import url(\''+ fonts +'\');\n</style>'
    //   }))
  	// 	.pipe(gulp.dest(job_path))
  	// 	.pipe(browsersync.stream());
    // }
  });
});


gulp.task('mqpacker', ['inline'], function(){
	var job_path = path.builds + '/' + currentjob.vertical + '/' + currentjob.campaign_year + '/' + currentjob.client + '/' + currentjob.campaign_month + '/' + currentjob.campaign_name;

	return gulp.src(job_path + '/css/email.css')
		.pipe(postcss([mqpacker]))
		.pipe(gulp.dest(job_path + '/final/test.css'))
});

// Updates Deliverable HTML
gulp.task('html-update', function(){
	var job_path = path.builds + '/' + currentjob.vertical + '/' + currentjob.campaign_year + '/' + currentjob.client + '/' + currentjob.campaign_month + '/' + currentjob.campaign_name;

	return gulp.src(job_path + '/index.html')
		.pipe(smoosher())
		.pipe(gulp.dest(job_path + '/final'))
		.pipe(browsersync.stream());
});

// Updates Deliverable Images
// * Needs to mirror deletion of files
gulp.task('clean-img', function(){
	var job_path = path.builds + '/' + currentjob.vertical + '/' + currentjob.campaign_year + '/' + currentjob.client + '/' + currentjob.campaign_month + '/' + currentjob.campaign_name;

	return gulp.src(job_path + '/final/images/**/*')
    .pipe(clean())
});

// Updates Deliverable Images
// * Needs to mirror deletion of files
gulp.task('img-update', ['clean-img'], function(){
	var job_path = path.builds + '/' + currentjob.vertical + '/' + currentjob.campaign_year + '/' + currentjob.client + '/' + currentjob.campaign_month + '/' + currentjob.campaign_name;

	return gulp.src(job_path + '/images/*')
		.pipe(gulp.dest(job_path + '/final/images'))
		.pipe(browsersync.stream());
});

// Prompts for Email Vertical
gulp.task('getVertical', function() {
	var list = fs.readdirSync(path.master_templates);
	var v;

	return gulp.src('.current.json')
		.pipe(prompts.prompt({
				type: 'list',
				name: 'vertical',
				message: 'What vertical are you coding for?',
				choices: list
		}, function(res) {
			v = res.vertical;
		}))
		.pipe(jeditor(function(json){
      json.vertical = v;
			currentjob = json;
			return json;
		}))
		.pipe(gulp.dest('./'));
});

// Prompts for Email Client
gulp.task('getClient', function() {
	var list = fs.readdirSync(path.master_templates + "/" + currentjob.vertical);
	var c;

	return gulp.src('.current.json')
		.pipe(prompts.prompt({
				type: 'list',
				name: 'clients',
				message: 'Who are you coding for?',
				choices: list
		}, function(res) {
			c = res.clients;
		}))
		.pipe(jeditor(function(json){
      json.client = c;
			currentjob = json;
			return json;
		}))
		.pipe(gulp.dest('./'));
});

// Prompts for Email Client
gulp.task('getCampaignDates', function() {
	var year, month;

	return gulp.src('.current.json')
		.pipe(prompts.prompt([{
    			type: 'input',
    			name: 'campaign_year',
    			message: 'What is the campaign year? (e.g. 2019, 2020)'
    	},
    	{
    			type: 'input',
    			name: 'campaign_month',
    			message: 'What is the campaign month? (e.g. 03, 10)'
    	}], function(res) {
    		year = res.campaign_year;
    		month = res.campaign_month;
    	}))
    	.pipe(jeditor(function(json){
    		json.campaign_year = year;
    		json.campaign_month = month;
    		currentjob = json;
    		return json;
    	}))
  		.pipe(gulp.dest('./'));
});

// Prompts for Email Campaign Type
gulp.task('getCampaignType', function() {
	var list = fs.readdirSync(path.master_templates + "/" + currentjob.vertical + "/" + currentjob.client);
	var t;

	return gulp.src('.current.json')
    .pipe(prompts.prompt({
        type: 'list',
        name: 'email_type',
        message: 'What type of email are you coding?',
        choices: list
      }, function(res) {
  			t = res.email_type;
  		}))
  		.pipe(jeditor(function(json){
  			json.email_type = t;
  			currentjob = json;
  			return json;
  		}))
  		.pipe(gulp.dest('./'));
});

// Prompts for Email Campaign Name
gulp.task('getCampaignName', function() {
	var n;

	return gulp.src('.current.json')
    .pipe(prompts.prompt({
    			type: 'input',
    			name: 'campaign_name',
    			message: 'What is the campaign name? (e.g. match-day, generic)',
    			validate: function(campaign_name) {
    		      var validatename = validate(campaign_name);
    		      if (!validatename.validForNewPackages) { return false; }
    		      return true;
    		    }
    	}, function(res) {
  			n = res.campaign_name;
  		}))
  		.pipe(jeditor(function(json){
  			json.campaign_name = n;
  			currentjob = json;
  			return json;
  		}))
  		.pipe(gulp.dest('./'));
});

gulp.task('forward-checker', function(){
  if(currentjob.email_type == "forward") {
    runSequence('forward-info');
  }
  else {
    runSequence('getCampaignName', 'pieces');
  }
});

// This will point the user to either start a new job
// or start from an existing job
gulp.task('job', function() {

	var list = fs.readdirSync(path.builds);

	if (currentjob.vertical != "") {
		gulp.src('*')
			.pipe(prompts.prompt({
					type: 'list',
					name: 'job',
					message: 'Do you want to start a new email or continue where you left off?\n',
					choices: [ "new email", "continue" ]
			}, function(res) {
				if (res.job == "new email") {
					runSequence('email-buildr');
				}
				else {
					runSequence('continue');
				}
			}))
			.pipe(gulp.dest('./'));
	}
	else {
		runSequence('email-buildr');
	}
});

// Guides the user to a job that already exists and
// asks what they want to continue working on
gulp.task('continue', function(){
	var msg = "The last job you worked on was " + currentjob.campaign_name + " for " + currentjob.client + ".\n\nDid you want to continue working on that job? (y/n)\n";
	var nextpath, v, c, y, m, j;

	return gulp.src('.current.json')
		.pipe(prompts.prompt({
			name: 'continue',
			message: msg
		}, function(res){
			if (res.continue == "y") {
				runSequence('watch');
			}
			else {
				var list = fs.readdirSync(path.builds);

				gulp.src('.current.json') // Vertical
					.pipe(prompts.prompt({
							type: 'list',
							name: 'jobvertical',
							message: 'What vertical do you want to continue working on?\n',
							choices: list
					}, function(res) {
						v = res.jobvertical;
						nextpath = path.builds + '/' + v;
						list = fs.readdirSync(nextpath);

						gulp.src('.current.json') // Year
							.pipe(prompts.prompt({
									type: 'list',
									name: 'jobyear',
									message: 'Which year you want to work on?\n',
									choices: list
							}, function(res) {
								y = res.jobyear;
								nextpath = nextpath + '/' + y;
								list = fs.readdirSync(nextpath);

								gulp.src('.current.json') // Client
									.pipe(prompts.prompt({
											type: 'list',
											name: 'jobclient',
											message: 'Which client do you want to work on?\n',
											choices: list
									}, function(res) {
										c = res.jobclient;
										nextpath = nextpath + '/' + c;
										list = fs.readdirSync(nextpath);

										gulp.src('.current.json') // Month
											.pipe(prompts.prompt({
													type: 'list',
													name: 'jobmonth',
													message: 'What month?\n',
													choices: list
											}, function(res) {
												m = res.jobmonth;
												nextpath = nextpath + '/' + m;
												list = fs.readdirSync(nextpath);

												return gulp.src('.current.json') // Campaigns
													.pipe(prompts.prompt({
															type: 'list',
															name: 'job',
															message: 'What job?\n',
															choices: list
													}, function(res) {
														j = res.job;
													}))
													.pipe(jeditor(function(json){
														json.vertical = v;
														json.campaign_year = y;
														json.client = c;
														json.campaign_month = m;
														json.campaign_name = j;
														currentjob = json;
														return json;
													}))
													.pipe(gulp.dest('./')).on('end', function(){
														runSequence('watch');
													});
											}));
									}));
							}));
					}));
			}
		}));
});

gulp.task('forward-info', function(){
  var list = fs.readdirSync(path.builds + '/' + currentjob.vertical + '/' + currentjob.campaign_year + '/' + currentjob.client + '/' + currentjob.campaign_month);
	var nextpath, v, c, y, m, j, j_fwd, original, new_email;

  return gulp.src('.current.json')
    .pipe(prompts.prompt({
      type: 'list',
      name: 'fwd_job',
      message: 'What job would you like to add a forward piece to?\n',
      choices: list
    }, function(res) {
			j = res.fwd_job;
      original = nextpath + '/' + j;
    }))
    .pipe(jeditor(function(json){
      json.campaign_name = j;
      currentjob = json;
      return json;
    }))
    .pipe(gulp.dest('./')).on('end', function(){
      runSequence('forward-build');
    });
});

gulp.task('forward-build', function(){
  var job_path = path.builds + "/" + currentjob.vertical + "/" + currentjob.campaign_year + "/" + currentjob.client + "/" + currentjob.campaign_month + "/" + currentjob.campaign_name;
  var month_path = path.builds + "/" + currentjob.vertical + "/" + currentjob.campaign_year + "/" + currentjob.client + "/" + currentjob.campaign_month;
  var forward_path = job_path + '-forward';

  // Copies original email and creates a forward version
  return gulp.src([job_path + "/**/*"])
    .pipe(gulp.dest(month_path + '/' + currentjob.campaign_name + '-forward')).on('end', function(){
      // Renaming the campaign name on current.json
      gulp.src(['.current.json'])
        .pipe(jeditor(function(json){
          json.campaign_name = currentjob.campaign_name + '-forward';
          currentjob = json;
          return json;
        }))
        .pipe(gulp.dest('./')).on('end', function(){
          // Adding Forward piece to the email body
          gulp.src(forward_path + '/index.html')
            .pipe(insertLines({
              'before': /<!-- Header Area -->$/,
              'lineBefore': '{% include ".master-email-framework/parts/forward/forward.html" %}'
            }))
            .pipe(nunjucksRender())
            .pipe(gulp.dest(forward_path)).on('end', function(){
              // Moving Forward styles to job folder
              gulp.src(path.master_parts + '/forward/forward.less')
                .pipe(gulp.dest(forward_path + '/less/parts')).on('end', function(){
                  // Adding forward.less to be compiled with the styles
                  return gulp.src(forward_path + '/less/inline.less')
                    .pipe(insertLines({
                      'before': /@import "theme\.less";$/,
                      'lineBefore': '@import "parts/forward.less";'
                    }))
                    .pipe(gulp.dest(forward_path + '/less')).on('end', function(){
                      runSequence('watch');
                    });
                });
            });
          });
    });
});

// Collects the default layout pieces if Default is chosen.
// If not, a new layout prompt appears.
gulp.task('pieces', function(){

	var obj = JSON.parse(fs.readFileSync(path.master_templates + "/" + currentjob.vertical + "/" + currentjob.client + "/" + currentjob.email_type + "/template.json"));
	var temp_arr = [];

	for( var i in obj.default_pieces ) {
		for ( var j in obj.default_pieces[i] ) {
			temp_arr.push(j);
		}
	}

	var msg = "For this email you can choose to use a\ndefault layout or create a custom layout.\nThe default layout includes:\n\n" + temp_arr.join("\n") + "\n\n" + "Would you like to use the default layout\nor create a new layout?";

	return gulp.src('*')
		.pipe(prompts.prompt({
				type: 'list',
				name: 'pieces',
				message: msg,
				choices: ["default", "new"]
		}, function(res){
			if(res.pieces == "default") {
				for( var i in obj.default_pieces ) {
					for ( var j in obj.default_pieces[i] ) {
						arr.push(j);
						paths.push(obj.default_pieces[i][j]);
					}
				}
				runSequence('build_folders');
			}
			else {
				runSequence('template_pieces');
			}
		}));
});

// Prompts the user for email pieces in the order the appear
// in the layout.
gulp.task('template_pieces', function(){

	var obj = JSON.parse(fs.readFileSync(path.master_templates + "/" + currentjob.vertical + "/" + currentjob.client + "/" + currentjob.email_type + "/template.json"));
	var temp_arr = [];

	for( var i in obj.template_pieces ) {
		temp_arr.push(i);
	}

	gulp.src('.current.json')
		.pipe(prompts.prompt({
				type: 'list',
				name: 'pieces',
				message: 'Choose in order, the pieces of the email. Choose DONE when done.\n',
				choices: temp_arr
		}, function(res){
			if(res.pieces != "done") {
				arr.push(res.pieces);
				runSequence('template_pieces');
			}
			else {
				for( var i = 0; i < arr.length; i++ ) {
					paths.push(obj.template_pieces[arr[i]]);
				}
				runSequence('build_folders');
			}
		}));
});

// Writing out the LESS files to be compiled in order
gulp.task('add_imports', function(cb){

	var job_path = path.builds + '/' + currentjob.vertical + '/' + currentjob.campaign_year + '/' + currentjob.client + '/' + currentjob.campaign_month + '/' + currentjob.campaign_name;

	var client_path = path.master_templates + "/" + currentjob.vertical + "/" + currentjob.client + "/" + currentjob.email_type;

	var email = '@import "email";\n\n';

	email += '@import "variables.less";\n\n';

	email += '@import "' + path.master_less + '/base.less";\n\n';

	// Parts of the Email
  fs.stat(job_path + '/less/parts', function(err, stats) {
    if(!err) {
      var parts = fs.readdirSync(job_path + '/less/parts/');
  		for ( var i = 0; i < parts.length; i++ ) {
  			email += '@import "parts/'+ parts[i] + '";\n';
  		}

      email += '\n@import "' + client_path + '/wrapper.less";\n\n';

    	email += '\n@import "theme.less";\n\n';

    	fs.writeFile(job_path + '/less/inline.less', email, function(){ cb(); });
    }
  });
});

// Build out the Job Folders
gulp.task('build_folders', function(cb){
	var client_path = path.master_templates + "/" + currentjob.vertical + "/" + currentjob.client + "/" + currentjob.email_type;
	var job_path = path.builds + "/" + currentjob.vertical + "/" + currentjob.campaign_year + "/"  + currentjob.client + "/" + currentjob.campaign_month + "/" + currentjob.campaign_name;

	// Makes and moves the parts style
	var less_list = fs.readdirSync(path.master_parts);

	for ( var i = 0; i < less_list.length; i++ ) {
		for ( var j = 0; j < arr.length; j++ ) {
			if(less_list[i] == arr[j]) {
				gulp.src(path.master_parts + "/" + less_list[i] + "/*.less")
					.pipe(gulp.dest(job_path + '/less/parts'));
				break;
			}
		}
	}

	gulp.src([client_path + "/variables.less", client_path + '/theme.less'])
		.pipe(gulp.dest(job_path + '/less')).on('end', function(){
      gulp.src(client_path + "/images/*")
      	.pipe(gulp.dest(job_path + '/images'))
      	.pipe(gulp.dest(job_path + '/final/images')).on('end', function(){
      		runSequence('add_imports', 'build');
      	});
    });

});

// Template creator
gulp.task('build', function(){

	var job_path = path.builds + "/" + currentjob.vertical + "/" + currentjob.campaign_year + "/" + currentjob.client + "/" + currentjob.campaign_month + "/" + currentjob.campaign_name;
	var thelayout = path.master_templates + "/" + currentjob.vertical + "/" + currentjob.client + "/" + currentjob.email_type + "/layout.html";
  var currentYear = currentjob.campaign_year;
	nunjucksRender.nunjucks.configure([path.master_parts]);

	gulp.src(path.master_parts + '/index.html')
		.pipe(nunjucksRender({
			data: {
				layout: thelayout,
				email_parts: paths,
        job_year: currentYear
			}
		}))
		.pipe(gulp.dest(job_path))
		.pipe(browsersync.stream());

	runSequence('watch');
});

// Notifications
////////////////////////

// Success notification
var lessSuccess = {
  title: 'Less',
  message: 'Less successfully compiled!',
  sound: "Pop"
}

// // Success notification
var inlineSuccess = {
  title: 'Inline',
  message: 'Your styles have been inlined!',
  sound: "Pop"
}

// // Error notification
var lessError = function(err) {
  notify.onError({
    title:    "Less",
    subtitle: "Less compile failure!",
    message:  "Error: <%= error.message %>",
    sound:    "Beep"
  })(err);

  this.emit('end');
};

// // Error notification
var inlineError = function(err) {
  notify.onError({
    title:    "Inline",
    subtitle: "Your code has NOT been inlined!",
    message:  "Error: <%= error.message %>",
    sound:    "Beep"
  })(err);

  this.emit('end');
};


gulp.task('default', function(){
	fs.stat('.current.json', function(err, stats){
		if(err) {
			var job = '{ "vertical": "", "email_type": "", "campaign_year": "", "client": "", "campaign_month": "", "campaign_name": "" }';
			fs.writeFileSync('.current.json', job);
		}
		currentjob = require('./.current.json');
		runSequence(['watch']);
	});
});

gulp.task('email', function(){
	fs.stat('.current.json', function(err, stats){
		if(err) {
			var job = '{ "vertical": "", "email_type": "", "campaign_year": "", "client": "", "campaign_month": "", "campaign_name": "" }';
			fs.writeFileSync('.current.json', job);
		}
		currentjob = require('./.current.json');
		runSequence(['job']);
	});
});

// The Email Buildr
gulp.task('email-buildr', function(cb) {
	runSequence('getVertical', 'getClient', 'getCampaignDates', 'getCampaignType', 'forward-checker' );
	return cb();
});
